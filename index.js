const { Plugin } = require("powercord/entities");
const { getModule } = require("powercord/webpack");
const { levenshteinDistance } = require("./levenshtein");
const levenshtein = false;

const member = {"avatar": "b023710b0990dcfb599f761ff4998647", "discriminator": "0001", "id": "340473152259751936", "username": "vinceh121", "publicFlags": 0};

const words = [
//	"bi",
	"crossdress",
	"vince", "vincent", "hyvert",
	"vinceh121", "vinceh121#0001",
	"340473152259751936", "<:bioutiful:792950055380058142>",
	"<:bi:791009150569283675>", "<:welp:791075609018630164>"
];
const users = [
];

window.pingTrig = { users, words };

const shouldPing = (t) => {
	if (t.author && users.includes(t.author.id)) // user pings
		return true;

	if (t.content) {
		const arr = t.content.toLowerCase().match(/[a-zA-Z0-9<>:#]+/g); // have `:`, `<` or `>` for emotes and `#` for tag
		if (arr) {
			for (let word of arr) { // word pings
				for (let pword of words) {
					if (levenshtein) {
						if (pword.length == 2) {
							if (pword === word) {
								return true
							}
						} else {
							if (levenshteinDistance(word, pword) <= 1) {
								t.content += " (guessing " + pword + " from " + word + ")";
								return true;
							}
						}
					} else {
						if (pword === word) {
							return true;
						}
					}
				}
			}
		}
	}
};

const intercept = (c) => {
	if ((c.type === "MESSAGE_CREATE" || c.type === "MESSAGE_UPDATE") 
		&& shouldPing(c.message)) {
		c.message.mentions.push(member);
	}
};

module.exports = class PingWord extends Plugin {
	startPlugin() {
		getModule(["setInterceptor"], false).setInterceptor(intercept);
	}
};

