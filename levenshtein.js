
function levenshteinMatrix(s, t) {
	let d = [];
	
	// zero out the matrix
	for (let i = 0; i < s.length; i++) {
		d[i] = new Array(t.length).fill(0);
	}

	// simplificating prefixes
	for (let i = 1; i < s.length; i++) {
		d[i][0] = i;
	}
	for (let j = 1; j < t.length; j++) {
		d[0][j] = j;
	}

	for (let j = 1; j < t.length; j++) {
		for (let i = 1; i < s.length; i++) {
			const subCost = s[i] == t[j] ? 0 : 1;

			d[i][j] = Math.min(
				d[i - 1][j] + 1,
				d[i][j - 1] + 1,
				d[i - 1][j - 1] + subCost);
		}
	}

	return d;
}

function levenshteinDistance(s, t) {
	return levenshteinMatrix(s, t)[s.length - 1][t.length - 1];
}

module.exports = { levenshteinMatrix, levenshteinDistance };

